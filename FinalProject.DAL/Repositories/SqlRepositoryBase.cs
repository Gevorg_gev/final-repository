﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FinalProject.Core.Repositories;
using FinalProject.Models.DB;

namespace FinalProject.DAL.Repositories
{
    public class SqlRepositoryBase<T> : ISqlRepository<T> where T : class
    {
        private readonly NORTHWNDContext nORTHWNDContext;
        public SqlRepositoryBase(NORTHWNDContext nORTHWNDContext)
        {
            this.nORTHWNDContext = nORTHWNDContext; 
        }

        public T Add(T entity)
        {
            nORTHWNDContext.Set<T>().Add(entity);
            return entity;
        }

        public void Update(T entity)
        {
            nORTHWNDContext.Set<T>().Update(entity); 
        }

        public T Get(int id)
        {
            return nORTHWNDContext.Set<T>().Find(id); 
        }

        public IEnumerable<T> GetWhere(Func<T, bool> func)
        {
            return nORTHWNDContext.Set<T>().Where(func);
        }

        public void Remove(T entity)
        {
            nORTHWNDContext.Set<T>().Remove(entity); 
        }
    }
}
