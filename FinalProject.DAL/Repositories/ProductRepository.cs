﻿using FinalProject.Core.Repositories;
using FinalProject.Models.DB;
using System;
using System.Collections.Generic;
using System.Text;
using FinalProject.DAL;

namespace FinalProject.DAL.Repositories
{
    public class ProductRepository : SqlRepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(NORTHWNDContext nORTHWNDContext) : base(nORTHWNDContext) 
        {
        }
    }
}
