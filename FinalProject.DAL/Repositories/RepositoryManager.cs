﻿using FinalProject.Core.Repositories;
using FinalProject.Models.DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.DAL.Repositories
{
    public class RepositoryManager : IRepositoryManager
    {
        private readonly NORTHWNDContext nORTHWNDContext; 
        public RepositoryManager(NORTHWNDContext nORTHWNDContext)
        {
            this.nORTHWNDContext = nORTHWNDContext; 
        } 
        private IProductRepository _products;
        public IProductRepository Products => _products ?? (_products = new ProductRepository(nORTHWNDContext));

        public ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted)
        {
            return SqlTransaction.Begin(nORTHWNDContext, isolation); 
        }

        public int SaveChanges()
        {
            return nORTHWNDContext.SaveChanges(); 
        }

        public Task<int> SaveChangesAsync()
        {
            return nORTHWNDContext.SaveChangesAsync(); 
        }
    }
} 
