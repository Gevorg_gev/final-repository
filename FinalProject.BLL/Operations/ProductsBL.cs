﻿using FinalProject.Core.Operations;
using FinalProject.Models.DB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using FinalProject.Controllers;

namespace FinalProject.BLL.Operations
{
    public class ProductsBL : IProductsBL 
    {
        private readonly NORTHWNDContext northWndContext; 
        public Product AddProducts(Product product) 
        {
            northWndContext.Products.Add(product);
            northWndContext.SaveChanges();  


            return product; 
        }

        public IEnumerable GetProducts()
        {
            throw new NotImplementedException();
        }

        public bool RemoveProducts(int id)
        {
            var rmv = northWndContext.Products.Find(id);
            if (rmv == null)
            {
                return false;
            }

            northWndContext.Products.Remove(rmv);
            northWndContext.SaveChanges();


            return true; 
        }

        public bool UpdateProducts(Product product, int id)
        {
            var productToUpdate = northWndContext.Products.Find(id);
            if (productToUpdate == null)
            {
                return false; 
            }


            productToUpdate.ProductId = product.ProductId;
            productToUpdate.ProductName = product.ProductName;
            productToUpdate.SupplierId = product.SupplierId;
            productToUpdate.CategoryId = product.CategoryId;
            productToUpdate.QuantityPerUnit = product.QuantityPerUnit;
            productToUpdate.UnitPrice = product.UnitPrice;
            productToUpdate.UnitsInStock = product.UnitsInStock;
            productToUpdate.UnitsOnOrder = product.UnitsOnOrder;
            productToUpdate.ReorderLevel = product.ReorderLevel;
            productToUpdate.Discontinued = product.Discontinued;

            northWndContext.Products.Update(productToUpdate);
            northWndContext.SaveChanges();


            return true;  
        }
    }
}
