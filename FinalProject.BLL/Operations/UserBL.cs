﻿using FinalProject.Core.Entities;
using FinalProject.Core.Exception;
using FinalProject.Core.Models;
using FinalProject.Core.Operations;
using FinalProject.Core.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.BLL.Operations
{
    public class UserBL : IUserBL 
    {
        private readonly IRepositoryManager _repositories;

        public UserBL(IRepositoryManager repositories)
        {
            _repositories = repositories;
        }
        public Task LoginAsync(UserLoginModel loginModel, HttpContext httpContext)
        {
            return httpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public Task LogOutAsync(HttpContext httpContext)
        {
            return httpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }

        public async Task<UserViewModel> RegisterAsync(UserRegisterModel registerModel, HttpContext httpContext)
        {
            var users = _repositories.Users.GetWhere(x => x.Username == registerModel.UserName);

            if (users.Any())
            {
                throw new LogicException("Username is already taken");
            }

            var user = new User
            {
                Username = registerModel.UserName,
                Password = registerModel.Password
            };

            _repositories.Users.Add(user); 

            await _repositories.SaveChangesAsync();

            await Authenticate(user, httpContext);

            return new UserViewModel
            {
                Id = user.Id,
                Username = user.Username
            };
        }

        private async Task Authenticate(User user, HttpContext httpContext)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Username),
                new Claim("Id",user.Id.ToString())
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);

            await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));
        }
    }
}
