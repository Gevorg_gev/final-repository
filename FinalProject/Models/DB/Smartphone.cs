﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FinalProject.Models.DB
{
    public partial class Smartphone
    {
        public int Id { get; set; }
        public string Model { get; set; }
        public string Color { get; set; }
        public string Brand { get; set; }
        public string Country { get; set; }
        public string Display { get; set; }
        public string Camera { get; set; }
        public string ScreenResolution { get; set; }
    }
}
