﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FinalProject.Models.DB
{
    public partial class ViewName
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
    }
}
