﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FinalProject.Models.DB
{
    public partial class InformaTion
    {
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
