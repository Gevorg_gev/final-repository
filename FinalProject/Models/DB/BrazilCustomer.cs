﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FinalProject.Models.DB
{
    public partial class BrazilCustomer
    {
        public string CompanyName { get; set; }
        public string ContactName { get; set; }
    }
}
