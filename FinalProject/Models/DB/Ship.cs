﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FinalProject.Models.DB
{
    public partial class Ship
    {
        public string Name { get; set; }
        public string Class { get; set; }
        public short? Launched { get; set; }
    }
}
