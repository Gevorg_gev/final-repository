﻿using System;
using System.Collections.Generic;

#nullable disable

namespace FinalProject.Models.DB
{
    public partial class Freight
    {
        public int Id { get; set; }
        public int Massive { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    }
}
