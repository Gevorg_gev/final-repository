using FinalProject.Models.DB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


[ApiController]
[Route("[controller]")]
public class ProductsController : ControllerBase
{
    private readonly NORTHWNDContext northWndContext;

    public ProductsController(NORTHWNDContext northWndContext)
    {
        this.northWndContext = northWndContext; 
    } 

    [HttpGet]
    public IActionResult GetProductsCountByCategory()
    {
                   var productsCountByCategory = northWndContext.Products
                                          .Join(northWndContext.Categories, x => x.CategoryId, y => y.CategoryId, (x, y) => new { y.CategoryName })
                                          .GroupBy(x => x.CategoryName)
                                          .Select(x => new { x.Key, ProductCount = x.Count() })
                                          .OrderByDescending(x => x.ProductCount)
                                          .ToArray();

        return Ok(productsCountByCategory); 
    } 

    [HttpGet]
    public IActionResult GetReorderedProducts()
    {
          var reorderedProducts = northWndContext.Products
                                    .Where(x => x.UnitsInStock < x.ReorderLevel)
                                    .GroupBy(x => new { x.UnitsInStock, x.ReorderLevel, x.ProductId, x.ProductName })
                                    .Select(x => new { x.Key.ProductId })
                                    .OrderBy(x => x.ProductId)
                                    .ToArray();

        return Ok(reorderedProducts); 
    }

    [HttpGet]
    public IActionResult GetReorderedProductsContinue()
    {
        var reorderedProductsContinue = northWndContext.Products
                                            .Where(x => x.UnitsInStock + x.UnitsOnOrder <= x.ReorderLevel && x.Discontinued == false)
                                            .GroupBy(x => new { x.UnitsInStock, x.UnitsOnOrder, x.ReorderLevel, x.Discontinued })
                                            .Select(x => new { x.Key.UnitsInStock, x.Key.UnitsOnOrder })
                                            .ToArray();

        return Ok(reorderedProductsContinue); 
    }

    [HttpGet]
    public IActionResult GetProducts()
    {
        var products = northWndContext.Products.ToList(); 
        return Ok(products);
    } 

    [HttpPost]
    public IActionResult AddProducts([FromBody] Product product)
    {
        northWndContext.Add(product);


        return Ok();
    }

    [HttpDelete("{id}")]
    public IActionResult RemoveProducts([FromRoute] int id)
    {
        northWndContext.Remove(id); 


        return Ok();
    }

    [HttpPut("{id}")]
    public IActionResult UpdateProducts([FromBody] Product product, [FromRoute] int id)
    { 
        northWndContext.Update(id); 


        return Ok();
    }
}