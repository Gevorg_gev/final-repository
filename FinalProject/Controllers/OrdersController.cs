using FinalProject.Models.DB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


[ApiController]
[Route("[controller]")] 

public class OrdersController : ControllerBase
{
    private readonly NORTHWNDContext northWndContext;

    public OrdersController(NORTHWNDContext northWndContext)
    {
        this.northWndContext = northWndContext;
    } 

    [HttpGet]
    public IActionResult GetHighFreightCharges()
    {
        var hightFreightCharges = northWndContext.Orders 
                                     .GroupBy(x => x.ShipCountry, x => x.Freight)
                                     .Select(x => new { x.Key })
                                     .Take(3)
                                     .ToArray();

        return Ok(hightFreightCharges); 
    }

    [HttpGet]
    public IActionResult GetHighFreightChargesIn1996()
    {
        var highFreightChargesIn1996 = northWndContext.Orders  
                                     .Where(x => x.OrderDate.Value.Year == 1996)
                                     .GroupBy(x => x.ShipCountry)
                                     .Select(x => new { x.Key })
                                     .Take(3)
                                     .ToArray();

        return Ok(highFreightChargesIn1996); 
    }
}