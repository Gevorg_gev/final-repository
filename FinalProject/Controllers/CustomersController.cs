using FinalProject.Models.DB;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


[ApiController]
[Route("[controller]")] 

public class CustomersController : ControllerBase
{
    private readonly NORTHWNDContext northWndContext;

    public CustomersController(NORTHWNDContext northWndContext)
    {
        this.northWndContext = northWndContext;
    } 

    [HttpGet]
    public IActionResult GetCustomersCountByCountryAndCity()
    {
        var customersCountByCountryAndCity = northWndContext.Customers
                                             .GroupBy(x => new { x.Country, x.City })
                                             .Select(x => new { x.Key.Country, x.Key.City, CustomersCount = x.Count() })
                                             .ToArray();

        return Ok(customersCountByCountryAndCity); 
    }

    [HttpGet]
    public IActionResult GetNoOrderCustomers()
    {
        var customers = northWndContext.Customers.Take(10).ToList(); 
        return Ok(customers); 
    }


}