﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using FinalProject.Models.DB;

namespace FinalProject.Core.Operations
{
    public interface IProductsBL
    {
        IEnumerable GetProducts();
        bool RemoveProducts(int id);
        bool UpdateProducts(Product product, int id);
        Product AddProducts(Product product); 
    }
}
