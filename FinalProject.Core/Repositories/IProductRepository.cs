﻿using FinalProject.Models.DB;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProject.Core.Repositories
{
    public interface IProductRepository : ISqlRepository<Product> 
    {

    }
}
