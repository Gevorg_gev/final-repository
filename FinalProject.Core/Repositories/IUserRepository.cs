﻿using FinalProject.Core.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProject.Core.Repositories
{
    public interface IUserRepository : ISqlRepository<User>
    {
    } 
}
