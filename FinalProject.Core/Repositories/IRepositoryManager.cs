﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace FinalProject.Core.Repositories
{
    public interface IRepositoryManager
    {
        IProductRepository Products { get; } 
        int SaveChanges();
        IUserRepository Users { get; } 
        Task<int> SaveChangesAsync();

        ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted);
    }
}
