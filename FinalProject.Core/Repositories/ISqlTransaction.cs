﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProject.Core.Repositories
{
    public interface ISqlTransaction : IDisposable 
    {
        void Commit();
        void Rollback();
    }
}
