﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProject.Core.Exception
{
    public class LogicException : RankException 
    {
        public LogicException(string message) : base(message)
        {
        }

    }
}
