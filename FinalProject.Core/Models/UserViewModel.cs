﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FinalProject.Core.Models
{
    public class UserViewModel
    {
        public int Id { get; set; }
        public string Username { get; set; } 
    }
}
